package ist.challenge.alexandergunawan.entities;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique=true)
    @NotBlank(message = "Username tidak boleh kosong")
    @Size(max = 25, message = "Panjang username tidak boleh melebihi 25 character")
    private String username;

    @NotBlank(message = "Password tidak boleh kosong")
    @Size(max = 25, message = "Panjang password tidak boleh melebihi 25 character")
    private String password;
}