package ist.challenge.alexandergunawan.services;
import ist.challenge.alexandergunawan.entities.User;
import ist.challenge.alexandergunawan.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        Optional<User> userFound = userRepository.findByUsernameIs(user.getUsername());
        if (!userFound.isPresent()) {
            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public Boolean login(User user) {
        Optional<User> userFound = userRepository.findByUsernameIs(user.getUsername());
        if (userFound.isPresent()) {
            if (userFound.get().getPassword().equals(user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    public List<User> getUserList() {
        return userRepository.findAll();
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public Boolean checkUsername(User user, Long id) {
        Optional<User> userFound = userRepository.findFirstByUsernameIsAndIdNot(user.getUsername(), id);

        if (userFound.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public User updateUser(User userBase, User user) {

        userBase.setUsername(user.getUsername());
        userBase.setPassword(user.getPassword());
        return userRepository.save(userBase);

    }

    public String deleteUserById(Long id) {
        userRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
    
}
