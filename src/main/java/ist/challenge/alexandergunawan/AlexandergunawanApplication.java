package ist.challenge.alexandergunawan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlexandergunawanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlexandergunawanApplication.class, args);
	}

}
