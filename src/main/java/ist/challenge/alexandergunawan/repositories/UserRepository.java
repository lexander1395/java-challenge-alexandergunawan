package ist.challenge.alexandergunawan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ist.challenge.alexandergunawan.entities.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByUsernameIs(String username);
  Optional<User> findById(Long id);
  Optional<User> findFirstByUsernameIsAndIdNot(String username, Long id);
}
