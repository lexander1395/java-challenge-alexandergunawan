package ist.challenge.alexandergunawan.controllers;

import ist.challenge.alexandergunawan.entities.User;
import ist.challenge.alexandergunawan.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService service;
    
    @GetMapping("/users/list")//you can give this any name you want and after adding this string to the end of base url you can use this
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(service.getUserList());
    }

    @PostMapping("/users/registrasi")
    public ResponseEntity<String> addUser(@RequestBody User user) {
        User userCreated = this.service.createUser(user);
        if (userCreated != null) {
            return ResponseEntity.status(201)
            .body("Registrasi berhasil");
        } else {
            return ResponseEntity.status(409)
            .body("Username sudah terpakai");
        }
    }

    @PostMapping("/users/login")
    public ResponseEntity<String> login(@RequestBody User user) {
        if(user.getUsername().isEmpty() || user.getPassword().isEmpty()){
            return ResponseEntity.status(400)
            .body("Username dan / atau password kosong");
        }
        Boolean loggedIn = this.service.login(user);
        if (loggedIn) {
            return ResponseEntity.status(200)
            .body("Sukses login");
        } else {
            return ResponseEntity.status(400)
            .body("Password salah");
        }
    }

    @PutMapping("/users/edit/{id}")
    public ResponseEntity<String> updateUser(
        @PathVariable Long id,
        @RequestBody User user
    ) {
        User userBase = this.service.getUserById(id);

        if(userBase != null){

            Boolean sameUsername = this.service.checkUsername(user,id);

            if(sameUsername){
                return ResponseEntity.status(409)
                .body("Username sudah terpakai");
            }

            Boolean samePassword = userBase.getPassword().equals(user.getPassword());

            if(samePassword){
                return ResponseEntity.status(400)
                .body("Password tidak boleh sama dengan password sebelumnya");
            }

            this.service.updateUser(userBase, user);
            return ResponseEntity.status(201)
            .body("User berhasil diedit");

        }else{
            return ResponseEntity.status(400)
            .body("ID user tidak ditemukan");
        }
    }
}
